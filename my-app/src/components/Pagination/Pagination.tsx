import React from 'react';
import ReactPaginate from 'react-paginate';
import "../Pagination/Pagination.css"

type PaginationProps = {
  pages: number;
  setCurrentPage: (page: number) => void;
};

const Pagination:React.FC<PaginationProps> = ({pages , setCurrentPage}) => {
  return (
    <>
        <ReactPaginate
          containerClassName="pagination-main-container"
          pageClassName="pagination-list-main"
          previousClassName="pagination-list-main"
          previousLinkClassName="pagination-text pagination-container pagination-next-prev pagination-buttons"
          nextClassName="pagination-list-main"
          nextLinkClassName="pagination-text pagination-container pagination-next-prev pagination-buttons"
          pageLinkClassName="pagination-container pagination-text pagination-list pagination-buttons"
          activeClassName="pagination-active-button"
          breakLabel="..."
          nextLabel="следующая >"
          onPageChange={(event:any) => setCurrentPage(event.selected + 1)}
          pageRangeDisplayed={5}
          pageCount={pages}
          previousLabel="< предыдущая"
        />
    </>
  );
};

export default Pagination;
